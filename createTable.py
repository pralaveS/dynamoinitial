from __future__ import print_function # Python 2/3 compatibility
import boto3

dynamodb = boto3.resource('dynamodb', region_name='us-west-2', endpoint_url="https://dynamodb.us-west-2.amazonaws.com") ##https://dynamodb.us-west-2.amazonaws.com


table = dynamodb.create_table(
    TableName='speciality_synonyms_updated',
    KeySchema=[
        {
            'AttributeName': 'user_keyword',
            'KeyType': 'HASH'  #Partition key
        },
        {
            'AttributeName': 'speciality',
            'KeyType': 'RANGE'  #Sort key
        }
    ],
    AttributeDefinitions=[
        {
            'AttributeName': 'user_keyword',
            'AttributeType': 'S'
        },
        {
            'AttributeName': 'speciality',
            'AttributeType': 'S'
        },

    ],
    ProvisionedThroughput={
        'ReadCapacityUnits': 10,
        'WriteCapacityUnits': 10
    }
)

print("Table status:", table.table_status)
