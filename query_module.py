from flask import Flask, jsonify
from boto3.dynamodb.conditions import Key, Attr
import boto3

app = Flask(__name__)

## query string will be given in this format ,"need_a_dentist" and it will automically replace underscore with spaces
@app.route('/<string:query_string>')
def query_method(query_string):
	dynamodb = boto3.resource('dynamodb', region_name='us-west-2', endpoint_url="https://dynamodb.us-west-2.amazonaws.com")
	table = dynamodb.Table('speciality_synonyms_updated')
	##replace query string "_" with " ", as spaces won't be allow in URL
	query_string = query_string.replace("_"," ")
	response = table.query(
		KeyConditionExpression=Key('user_keyword').eq(query_string)
		)
	query_ans = []

	for i in response['Items']:
		print (i['user_keyword'], i['type_keywords'], i['search_keyword'])
		new_list = [i['user_keyword'], i['type_keywords'], i['search_keyword']]
		query_ans.append(new_list)
	return jsonify(query_ans) ## returns a json format list of list which contains user_keyword, type_keyword, search_keyword


if __name__ == '__main__':
	app.run(debug=True)