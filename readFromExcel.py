from textblob import TextBlob
import itertools
import xlrd
import boto3



workbook = xlrd.open_workbook('Specialty_synonyms_v0.0.3.xlsx')

worksheet = workbook.sheet_by_index(2)

dynamodb = boto3.resource('dynamodb',endpoint_url="https://dynamodb.us-west-2.amazonaws.com") ##https://dynamodb.us-west-2.amazonaws.com

table = dynamodb.Table('speciality_synonyms_updated')

user_keywords = []
specialities = []
search_keywords = []
type_keywords = []

for i in range(1, 7660):
    user_keyword_blob = TextBlob(worksheet.cell(i, 0).value)
    user_keyword_correct1 = user_keyword_blob.correct()
    user_keyword_correct = str(user_keyword_correct1)
    user_keywords.append(user_keyword_correct)
    specialities.append(worksheet.cell(i, 1).value)
    type_keywords.append(worksheet.cell(i, 2).value)
    search_keywords.append(worksheet.cell(i, 3).value)


for user_keyword, speciality, type_keyword, search_keyword in itertools.izip(user_keywords, specialities, type_keywords, search_keywords):
    if search_keyword == "":
        search_keyword = "-----"
    print ("Adding data,", user_keyword, speciality, search_keyword)
    table.put_item(
        Item={
            'user_keyword' : user_keyword,
            'speciality' : speciality,
            'type_keywords' : type_keyword,
            'search_keyword' : search_keyword,
            }
        )

